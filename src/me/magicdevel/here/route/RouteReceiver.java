package me.magicdevel.here.route;

import com.google.android.gms.maps.model.LatLng;

import java.util.HashMap;
import java.util.List;

/**
 * Created by sergejdorodnyh on 12.10.14.
 */
public interface RouteReceiver {
    public void onReceiveRoute(LatLng origin, LatLng destination, List<List<HashMap<String, String>>> route);
}
