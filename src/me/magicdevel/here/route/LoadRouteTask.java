package me.magicdevel.here.route;

import android.os.AsyncTask;
import android.util.Log;
import com.google.android.gms.maps.model.LatLng;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

/**
 * Created by sergejdorodnyh on 05.10.14.
 */
public class LoadRouteTask extends AsyncTask<Void, Integer, List<List<HashMap<String, String>>>> {
    private RouteReceiver mRouteReceiver;
    private LatLng mOrigin;
    private LatLng mDestination;

    public LoadRouteTask(RouteReceiver routeReceiver, LatLng origin, LatLng destination) {
        mRouteReceiver = routeReceiver;
        mOrigin = origin;
        mDestination = destination;
    }

    @Override
    protected List<List<HashMap<String, String>>> doInBackground(Void... params) {
        List<List<HashMap<String, String>>> route = null;

        try {
            String data = "";
            HttpConnection http = new HttpConnection();
            String url = getMapsApiDirectionsUrl(mOrigin, mDestination);
            Log.d("Load route", "Route url: " + url);
            data = http.readUrl(url);

            JSONObject jObject;

            try {
                jObject = new JSONObject(data);
                PathJSONParser parser = new PathJSONParser();
                route = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            Log.d("Load route", e.toString());
        }

        return route;
    }

    private String getMapsApiDirectionsUrl(LatLng origin, LatLng destination) {
        // Origin of route
        String strOrigin = "origin=" + origin.latitude + "," + origin.longitude;

        // Destination of route
        String strDestination = "destination=" + destination.latitude + "," + destination.longitude;

        // Sensor enabled
        String sensor = "sensor=true";

        String mode = "mode=walking";

        // Building the parameters to the web service
        String parameters = strOrigin + "&" + strDestination + "&" + sensor + "&" + mode;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;

        return url;
    }

    @Override
    protected void onPostExecute(List<List<HashMap<String, String>>> route) {
        mRouteReceiver.onReceiveRoute(mOrigin, mDestination, route);
    }
}
