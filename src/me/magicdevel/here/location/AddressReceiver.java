package me.magicdevel.here.location;

/**
 * Created by sergejdorodnyh on 27.07.14.
 */
public interface AddressReceiver {
    public void onReceiveAddress(String address);

    public void onError();

    public void onAddressNotFound();

    public void onRequestFinish();
}
