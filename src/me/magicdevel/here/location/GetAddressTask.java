package me.magicdevel.here.location;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.util.Log;
import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

/**
 * Created by sergejdorodnyh on 27.07.14.
 */
public class GetAddressTask extends AsyncTask<LatLng, Void, String> {

    private Context mContext;
    private AddressReceiver mReceiver;
    private boolean mHasError = false;
    private boolean mAddressNotFound = false;

    public GetAddressTask(Context context, AddressReceiver receiver) {
        super();

        mContext = context;
        mReceiver = receiver;
    }

    @Override
    protected String doInBackground(LatLng... params) {
        Geocoder geocoder =
                new Geocoder(mContext, Locale.getDefault());
        // Get the current location from the input parameter list
        LatLng loc = params[0];
        // Create a list to contain the result address
        List<Address> addresses = null;
        try {
                /*
                 * Return 1 address.
                 */
            addresses = geocoder.getFromLocation(loc.latitude,
                    loc.longitude, 1);
        } catch (IOException e1) {
            Log.e("LocationSampleActivity",
                    "IO Exception in getFromLocation()");
            e1.printStackTrace();
            mHasError = true;
            return null;
        } catch (IllegalArgumentException e2) {
            // Error message to post in the log
            String errorString = "Illegal arguments " +
                    Double.toString(loc.latitude) +
                    " , " +
                    Double.toString(loc.longitude) +
                    " passed to address service";
            Log.e("GetAddressTask", errorString);
            e2.printStackTrace();
            mHasError = true;
            return null;
        }
        // If the reverse geocode returned an address
        if (addresses != null && addresses.size() > 0) {
            // Get the first address
            Address address = addresses.get(0);
                /*
                 * Format the first line of address (if available),
                 * city, and country name.
                 */
            String addressLine = address.getMaxAddressLineIndex() > 0 ?
                    address.getAddressLine(0) : "";
            String locality = address.getLocality();

            String addressText;

            if(!addressLine.isEmpty() && locality != null) {
                addressText = String.format("%s, %s", addressLine, locality);
            } else if(addressLine.isEmpty()) {
                addressText = locality;
            } else {
                addressText = addressLine;
            }

            // Return the text
            return addressText;
        } else {
            mAddressNotFound = true;
            return null;
        }
    }

    @Override
    protected void onPostExecute(String address) {
        if(address == null) {
            if(mHasError)
                mReceiver.onError();
            else if(mAddressNotFound)
                mReceiver.onAddressNotFound();
        } else {
            mReceiver.onReceiveAddress(address);
        }

        mReceiver.onRequestFinish();
    }
}
