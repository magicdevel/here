package me.magicdevel.here;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import me.magicdevel.here.action.FavoriteAction;
import me.magicdevel.here.action.HistoryAction;
import me.magicdevel.here.action.MapAction;
import me.magicdevel.here.action.MapFragmentActions;
import me.magicdevel.here.data.Db;
import me.magicdevel.here.data.FavoriteItem;
import me.magicdevel.here.data.HistoryItem;
import me.magicdevel.here.data.HistoryTable;
import me.magicdevel.here.data.exception.NotFound;
import me.magicdevel.here.marker.Marker;
import me.magicdevel.here.parcel.HasParcel;
import me.magicdevel.here.parcel.MapFragmentParcel;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: magicdevel
 * Date: 12.06.2014
 * Time: 13:36
 */
public class FragmentedActivity extends SlidingMapActivity implements
        MapAction,
        HistoryAction,
        MapFragmentActions,
        FavoriteAction {
    /**
     * Имена фрагметов, поддерживающих сохрание
     */
    public static final String FRAGMENT_NAMES[] = { "map" };
    public static final String FRAGMENTS_SETTINGS = "fragments";

    private HashMap<String, Parcelable> mFragmentParcels = new HashMap<String, Parcelable>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(savedInstanceState != null)
            loadFragmentsData(savedInstanceState);
        else
            loadFragmentsDataFromSettings();

        setContentView(R.layout.fragmented_main);
        setBehindContentView(R.layout.slidemenu);

//        ((Button)findViewById(R.id.ordinalButton)).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                attachOrdinalFragment();
//                toggle();
//            }
//        });
//
//        ((Button)findViewById(R.id.mapButton)).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                attachMapFragment();
//                toggle();
//            }
//        });

        SlidingMenu menu = getSlidingMenu();
        menu.setTouchModeAbove(SlidingMenu.LEFT);
        menu.setShadowWidth(25);
        menu.setShadowDrawable(R.drawable.shadow);
        menu.setBehindOffset(100);
        menu.setFadeDegree(0.35f);
//        getSlidingMenu().setMode(SlidingMenu.LEFT_RIGHT);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        setSlidingActionBarEnabled(false);

        Fragment restoredFragment = getSupportFragmentManager().findFragmentByTag("content");
        if(restoredFragment == null)
            attachMapFragment(false);
        else if(restoredFragment instanceof HasParcel)
            restoreFragmentData((HasParcel) restoredFragment);

        if(restoredFragment != null && restoredFragment instanceof FragmentWithTitle)
            getSupportActionBar().setTitle(((FragmentWithTitle) restoredFragment).getTitle());
    }

    protected void restoreFragmentData(HasParcel fragment) {
        Parcelable parcel = mFragmentParcels.get(fragment.getParcelName());
        if(parcel != null)
            fragment.applyParcel(parcel);
    }

    /**
     * Присоединение фрагмента с картой
     *
     * @param immediately если - true, то присоединение происходит сразу, а не в фоновом режиме (используется, чтобы затем выполнить какие-то действия над фрагментом)
     */
    protected void attachMapFragment(boolean immediately) {
        FragmentManager manager = getSupportFragmentManager();

        MapFragmentParcel parcel = mFragmentParcels.containsKey("map")
                ? (MapFragmentParcel)mFragmentParcels.get("map")
                : new MapFragmentParcel();

        if(isMapActivated()) {
            Log.d("fragmented activity", "map already attached");

            Fragment fragment = manager.findFragmentByTag("content");
            ((MapFragment) fragment).applyParcel(parcel);

            return;
        }

        saveFragmentData();

        manager.beginTransaction()
                .replace(R.id.contentContainer, new MapFragment(parcel), "content")
                .commit();

        getSupportActionBar().setTitle(R.string.map);

        if(immediately)
            manager.executePendingTransactions();
    }

    protected void attachHistoryFragment() {
        saveFragmentData();

        FragmentManager manager = getSupportFragmentManager();
        manager.beginTransaction()
                .replace(R.id.contentContainer, new HistoryFragment(), "content")
                .commit();

        getSupportActionBar().setTitle(R.string.recent);
    }

    protected void attachFavoritesFragment() {
        saveFragmentData();

        FragmentManager manager = getSupportFragmentManager();
        manager.beginTransaction()
                .replace(R.id.contentContainer, new FavoritesFragment(), "content")
                .commit();

        getSupportActionBar().setTitle(R.string.favorites);
    }

    protected boolean isMapActivated() {
        FragmentManager manager = getSupportFragmentManager();
        Fragment fragment = manager.findFragmentByTag("content");

        return fragment instanceof  MapFragment;
    }

    protected void saveFragmentData() {
        Fragment fragment = getSupportFragmentManager().findFragmentByTag("content");

        if(fragment == null) {
            Log.d("fragmented activity", "no fragment to save");

            return;
        }


        Log.d("fragmented activity", "fragment is found");

        if(fragment instanceof HasParcel) {

            Log.d("fragmented activity", "save a parcel");

            String name = ((HasParcel)fragment).getParcelName();
            Parcelable parcel = ((HasParcel)fragment).getParcel();

            mFragmentParcels.put(name, parcel);
        } else {
            Log.d("fragmented activity", "this fragment has not a parcel");
        }
    }

    protected void saveFragmentsData(Bundle outState) {
        Log.d("fragmented activity", "saving state");

        for(Map.Entry<String, Parcelable> entry : mFragmentParcels.entrySet()) {
            String key = entry.getKey();
            Parcelable value = entry.getValue();

            outState.putParcelable(key, value);

            Log.d("fragmented activity", "saved '" + key + "'");
        }
    }

    protected void loadFragmentsData(Bundle outState) {
        for(String name : FRAGMENT_NAMES) {
            Parcelable parcel = outState.getParcelable(name);

            if(parcel != null)
                mFragmentParcels.put(name, parcel);
        }
    }

    protected void saveFragmentsDataInSettings() {
        SharedPreferences settings = getSharedPreferences(FRAGMENTS_SETTINGS, 0);
        SharedPreferences.Editor editor = settings.edit();

        Log.d("fragmented activity", "saving fragments in settings");

        for(Map.Entry<String, Parcelable> entry : mFragmentParcels.entrySet()) {
            Parcel parcel = Parcel.obtain();
            parcel.writeParcelable(entry.getValue(), 0);

            String value = Base64.encodeToString(parcel.marshall(), Base64.DEFAULT);
            editor.putString(entry.getKey(), value);

            parcel.recycle();
        }

        editor.commit();
    }

    protected void loadFragmentsDataFromSettings() {
        SharedPreferences settings = getSharedPreferences(FRAGMENTS_SETTINGS, 0);

        for(String name : FRAGMENT_NAMES) {
            String fragmentSettings = settings.getString(name, "");
            if(fragmentSettings.isEmpty())
                continue;

            byte data[] = Base64.decode(fragmentSettings, Base64.DEFAULT);

            Parcel parcel = Parcel.obtain();
            parcel.unmarshall(data, 0, data.length);
            parcel.setDataPosition(0);

            Parcelable fragmentParcel = parcel.readParcelable(getClassLoader());
            if(fragmentParcel != null)
                mFragmentParcels.put(name, fragmentParcel);

            parcel.recycle();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                toggle();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onHistoryAction() {
        attachHistoryFragment();
        toggle();
    }

    @Override
    public void onSelectHistory(final HistoryItem item) {
        attachMapFragment(true);

        FragmentManager manager = getSupportFragmentManager();

        ((MapFragment)manager.findFragmentByTag("content")).runAfterMapInit(new MapFragment.InitMapListener() {
            @Override
            public void onInitMap(MapFragment fragment, GoogleMap map) {
                LatLng position = new LatLng(item.getLat(), item.getLng());
                Marker marker = fragment.addMarkerIf(position);
                if (!marker.isSelected())
                    fragment.toggleMarker(marker);

                fragment.moveCameraTo(position, MapFragment.DEFAULT_ZOOM);
            }
        });
    }

    @Override
    public void onMapAction() {
        if(!isMapActivated())
            attachMapFragment(false);

        toggle();
    }

    @Override
    protected void onPause() {
        super.onPause();

        saveFragmentData();
        saveFragmentsDataInSettings();
    }

    @Override
    protected void onStop() {
        super.onStop();

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        saveFragmentsData(outState);
    }

    @Override
    public long onAddTarget(double lat, double lng) {
        Db db = new Db(this);

        try {
            long id = db.getHistoryTable().insert(lat, lng, null);

            saveFragmentData();
            saveFragmentsDataInSettings();

            return id;
        }
        finally {
            db.close();
            Log.d("FragmentedActivity", "Closed db after add a target location");
        }
    }

    @Override
    public void onUpdateTarget(long id, double lat, double lng) {
        Db db = new Db(this);

        try {
            HistoryTable table = db.getHistoryTable();
            HistoryItem item = table.find(id);
            item.setLat(lat);
            item.setLng(lng);
            item.setName(null);

            table.update(item);
        }
        catch (NotFound e) {

        }
        finally {
            db.close();
        }
    }

    @Override
    public long onAddFavorite(double lat, double lng) {
        Db db = new Db(this);

        try {
            try {
                return db.getFavoriteTable().findByLatLng(lat, lng).getId();
            }
            catch (NotFound e) {
                return db.getFavoriteTable().insert(lat, lng, null);
            }
        }
        finally {
            db.close();
            Log.d("FragmentedActivity", "Closed db after add a favorite location");
        }
    }

    @Override
    public void onFavoriteAction() {
        attachFavoritesFragment();
        toggle();
    }

    @Override
    public void onSelectFavorite(final FavoriteItem item) {
        attachMapFragment(true);

        FragmentManager manager = getSupportFragmentManager();

        ((MapFragment)manager.findFragmentByTag("content")).runAfterMapInit(new MapFragment.InitMapListener() {
            @Override
            public void onInitMap(MapFragment fragment, GoogleMap map) {
                LatLng position = new LatLng(item.getLat(), item.getLng());
                Marker marker = fragment.addMarkerIf(position);
                if (!marker.isSelected())
                    fragment.toggleMarker(marker);

                fragment.moveCameraTo(position, MapFragment.DEFAULT_ZOOM);
            }
        });

    }
}