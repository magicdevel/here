package me.magicdevel.here.action;

/**
 * Итерфейс для реакции на открытие карты
 */
public interface MapAction {
    public void onMapAction();
}
