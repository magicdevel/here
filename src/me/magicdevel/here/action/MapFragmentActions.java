package me.magicdevel.here.action;

/**
 * Created by magicdevel on 23.07.2014.
 */
public interface MapFragmentActions {
    /**
     * Обработка действия добавления целевой точки
     *
     * @param lat
     * @param lng
     * @return id добавленной точки
     */
    public long onAddTarget(double lat, double lng);

    /**
     * Обновление целевой точки
     *
     * @param id
     * @param lat
     * @param lng
     */
    public void onUpdateTarget(long id, double lat, double lng);

    /**
     * Добавлено в избранное
     *
     * @param lat
     * @param lng
     * @return id добавленной записи
     */
    public long onAddFavorite(double lat, double lng);
}
