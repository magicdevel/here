package me.magicdevel.here.action;

import me.magicdevel.here.data.HistoryItem;

/**
 * Итерфейс для реакции на открытие истории
 */
public interface HistoryAction {
    public void onHistoryAction();

    public void onSelectHistory(HistoryItem item);
}
