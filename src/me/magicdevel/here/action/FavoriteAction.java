package me.magicdevel.here.action;

import me.magicdevel.here.data.FavoriteItem;

/**
 * Created by sergejdorodnyh on 23.10.14.
 */
public interface FavoriteAction {
    public void onFavoriteAction();

    public void onSelectFavorite(FavoriteItem item);
}
