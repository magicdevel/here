package me.magicdevel.here;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ListFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.widget.CursorAdapter;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.*;
import android.widget.*;
import com.google.android.gms.maps.model.LatLng;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.PauseOnScrollListener;
import me.magicdevel.here.action.HistoryAction;
import me.magicdevel.here.data.*;
import me.magicdevel.here.location.AddressReceiver;
import me.magicdevel.here.location.GetAddressTask;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

/**
 * Created by magicdevel on 23.07.2014.
 */
public class HistoryFragment extends ListFragment implements LoaderManager.LoaderCallbacks<Cursor>, View.OnTouchListener, FragmentWithTitle {
    protected static final String STATE_PAUSE_ON_SCROLL = "STATE_PAUSE_ON_SCROLL";
    protected static final String STATE_PAUSE_ON_FLING = "STATE_PAUSE_ON_FLING";

    public static final int LOADER_ID = 1000;
    public static final float MAP_RATIO = 3.0f / 4.0f;
    public static final int MAP_ZOOM = 17;

    DisplayImageOptions mDisplayImageOptions;
    protected ImageLoader mImageLoader = ImageLoader.getInstance();

    private boolean mPauseOnScroll = false;
    private boolean mPauseOnFling = true;

    private boolean mRestorePosition = false;
    private int mRestoreIndex = 0;
    private int mRestoreTop = 0;

    float mDownX;
    private int mSwipeSlop = -1;

    boolean mSwiping = false;
    boolean mItemPressed = false;
    HashMap<Long, Integer> mItemIdTopMap = new HashMap<Long, Integer>();

    private int mMapHeight = 0;
    private int mMapWidth = 0;
    /**
     * Если true, то перед отображение карты image view будет очищаться.
     * false выставляется, когда нужно выполнить перерисовку без мерцания во время операции удаления
     */
    private boolean mCleanMapImage = true;

    private static final int SWIPE_DURATION = 250;
    private static final int MOVE_DURATION = 150;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mDisplayImageOptions = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .displayer(new FadeInBitmapDisplayer(500, true, true, false))
//                .displayer(new SimpleBitmapDisplayer())
                .build();

        getLoaderManager().initLoader(LOADER_ID, null, this);

        return inflater.inflate(R.layout.history, container, false);
    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);

        if(savedInstanceState != null) {
            mPauseOnScroll = savedInstanceState.getBoolean(STATE_PAUSE_ON_SCROLL, false);
            mPauseOnFling = savedInstanceState.getBoolean(STATE_PAUSE_ON_FLING, true);
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        applyScrollListener();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putBoolean(STATE_PAUSE_ON_SCROLL, mPauseOnScroll);
        outState.putBoolean(STATE_PAUSE_ON_FLING, mPauseOnFling);
    }

    private void applyScrollListener() {
        getListView().setOnScrollListener(new PauseOnScrollListener(mImageLoader, mPauseOnScroll, mPauseOnFling));
    }

    @Override
    public android.support.v4.content.Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Uri uri = HistoryProvider.CONTENT_URI;
        return new CursorLoader(getActivity(), uri, null, null, null, null);
    }

    @Override
    public void onLoadFinished(android.support.v4.content.Loader<Cursor> loader, Cursor data) {
        ListAdapter adapter = new ListAdapter(getActivity(), data);
        setListAdapter(adapter);

        if(mRestorePosition) {
            getListView().setSelectionFromTop(mRestoreIndex, mRestoreTop);
            mRestorePosition = false;
        }
    }

    @Override
    public void onLoaderReset(android.support.v4.content.Loader<Cursor> loader) {
        setListAdapter(null);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        Cursor cursor = (Cursor)getListAdapter().getItem(position);
        HistoryItem item = HistoryTable.createItem(cursor);
        if(getActivity() instanceof HistoryAction) {
            ((HistoryAction)getActivity()).onSelectHistory(item);
        }
    }

    @Override
    public boolean onTouch(final View v, MotionEvent event) {
        if (mSwipeSlop < 0) {
            mSwipeSlop = ViewConfiguration.get(getActivity()).
                    getScaledTouchSlop();
        }
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                if (mItemPressed) {
                    // Multi-item swipes not handled
                    return false;
                }
                mItemPressed = true;
                mDownX = event.getX();
                break;
            case MotionEvent.ACTION_CANCEL:
                v.setAlpha(1);
                v.setTranslationX(0);
                mItemPressed = false;
                break;
            case MotionEvent.ACTION_MOVE:
            {
                float x = event.getX() + v.getTranslationX();
                float deltaX = x - mDownX;
                float deltaXAbs = Math.abs(deltaX);
                if (!mSwiping) {
                    if (deltaXAbs > mSwipeSlop) {
                        mSwiping = true;
                        getListView().requestDisallowInterceptTouchEvent(true);
//                        mBackgroundContainer.showBackground(v.getTop(), v.getHeight());
                    }
                }
                if (mSwiping) {
                    v.setTranslationX((x - mDownX));
                    v.setAlpha(1 - deltaXAbs / v.getWidth());
                }
            }
            break;
            case MotionEvent.ACTION_UP:
            {
                // User let go - figure out whether to animate the view out, or back into place
                if (mSwiping) {
                    float x = event.getX() + v.getTranslationX();
                    float deltaX = x - mDownX;
                    float deltaXAbs = Math.abs(deltaX);
                    float fractionCovered;
                    float endX;
                    float endAlpha;
                    final boolean remove;
                    if (deltaXAbs > v.getWidth() / 4) {
                        // Greater than a quarter of the width - animate it out
                        fractionCovered = deltaXAbs / v.getWidth();
                        endX = deltaX < 0 ? -v.getWidth() : v.getWidth();
                        endAlpha = 0;
                        remove = true;
                    } else {
                        // Not far enough - animate it back
                        fractionCovered = 1 - (deltaXAbs / v.getWidth());
                        endX = 0;
                        endAlpha = 1;
                        remove = false;
                    }
                    // Animate position and alpha of swiped item
                    // NOTE: This is a simplified version of swipe behavior, for the
                    // purposes of this demo about animation. A real version should use
                    // velocity (via the VelocityTracker class) to send the item off or
                    // back at an appropriate speed.
                    long duration = (int) ((1 - fractionCovered) * SWIPE_DURATION);
                    getListView().setEnabled(false);
                    v.animate().setDuration(duration).
                            alpha(endAlpha).translationX(endX).
                            withEndAction(new Runnable() {
                                @Override
                                public void run() {
                                    // Restore animated values
                                    v.setAlpha(1);
                                    v.setTranslationX(0);
                                    if (remove) {
                                        animateRemoval(getListView(), v);
                                        Log.d("HistoryFragment", "remove item");
                                    } else {
//                                        mBackgroundContainer.hideBackground();
                                        mSwiping = false;
                                        getListView().setEnabled(true);
                                        Log.d("HistoryFragment", "cancel remove");
                                    }
                                }
                            });
                } else {
                    emulateItemClick(v);
                }
            }
            mItemPressed = false;

            break;
            default:
                return false;
        }
        return true;
    }

    private void emulateItemClick(View clickedView) {
        ListView listview = getListView();
        int firstVisiblePosition = listview.getFirstVisiblePosition();
        for (int i = 0; i < listview.getChildCount(); ++i) {
            View child = listview.getChildAt(i);
            if (child == clickedView) {
                int position = firstVisiblePosition + i;
                long itemId = getListView().getAdapter().getItemId(position);
                getListView().playSoundEffect(SoundEffectConstants.CLICK);
                onListItemClick(listview, clickedView, position, itemId);
                break;
            }
        }
    }

    /**
     * This method animates all other views in the ListView container (not including ignoreView)
     * into their final positions. It is called after ignoreView has been removed from the
     * adapter, but before layout has been run. The approach here is to figure out where
     * everything is now, then allow layout to run, then figure out where everything is after
     * layout, and then to run animations between all of those start/end positions.
     */
    private void animateRemoval(final ListView listview, final View viewToRemove) {
        int firstVisiblePosition = listview.getFirstVisiblePosition();
        for (int i = 0; i < listview.getChildCount(); ++i) {
            View child = listview.getChildAt(i);
            if (child != viewToRemove) {
                int position = firstVisiblePosition + i;
                long itemId = getListView().getAdapter().getItemId(position);
                mItemIdTopMap.put(itemId, child.getTop());
                Log.d("HistoryFragment", "Prepare for moving item with id " + itemId);
            }
        }

        // Delete the item from the adapter
        int position = listview.getPositionForView(viewToRemove);
        Cursor cursor = (Cursor)(listview.getAdapter()).getItem(position);
        Db db = new Db(getActivity());
        try {
            Log.d("HistoryFragment", "Removing history item with id " + cursor.getLong(cursor.getColumnIndex(HistoryTable.ID_FIELD)));
            db.getHistoryTable().delete(cursor.getLong(cursor.getColumnIndex(HistoryTable.ID_FIELD)));
        }
        finally {
            db.close();
        }

        final ListAdapter adapter = (ListAdapter)listview.getAdapter();
        Cursor cursorWithDelete = new CursorWithDelete(adapter.getCursor(), position);
        adapter.swapCursor(cursorWithDelete);


        mCleanMapImage = false;
        final ViewTreeObserver observer = listview.getViewTreeObserver();
        observer.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            public boolean onPreDraw() {
                observer.removeOnPreDrawListener(this);
                boolean firstAnimation = true;
                int firstVisiblePosition = listview.getFirstVisiblePosition();
                for (int i = 0; i < listview.getChildCount(); ++i) {
                    final View child = listview.getChildAt(i);
                    int position = firstVisiblePosition + i;
                    long itemId = listview.getAdapter().getItemId(position);

                    Log.d("HistoryFragment", "Moving item with id " + itemId);

                    Integer startTop = mItemIdTopMap.get(itemId);
                    int top = child.getTop();
                    if (startTop != null) {
                        Log.d("HistoryFragment", "Moving from " + startTop + " to " + top);
                        if (startTop != top) {
                            int delta = startTop - top;
                            child.setTranslationY(delta);
                            child.animate().setDuration(MOVE_DURATION).translationY(0);
                            if (firstAnimation) {
                                child.animate().withEndAction(new Runnable() {
                                    public void run() {
//                                        mBackgroundContainer.hideBackground();
                                        mSwiping = false;
                                        listview.setEnabled(true);
//                                        adapter.swapCursor(((CursorWithDelete)adapter.getCursor()).getCursor());
//                                        adapter.reload();
                                    }
                                });
                                firstAnimation = false;
                            }
                        }
                    } else {
                        // Animate new views along with the others. The catch is that they did not
                        // exist in the start state, so we must calculate their starting position
                        // based on neighboring views.
                        int childHeight = child.getHeight() + listview.getDividerHeight();
                        startTop = top + (i > 0 ? childHeight : -childHeight);
                        int delta = startTop - top;
                        child.setTranslationY(delta);
                        child.animate().setDuration(MOVE_DURATION).translationY(0);
                        if (firstAnimation) {
                            child.animate().withEndAction(new Runnable() {
                                public void run() {
//                                    mBackgroundContainer.hideBackground();
                                    mSwiping = false;
                                    listview.setEnabled(true);
//                                    adapter.swapCursor(((CursorWithDelete)adapter.getCursor()).getCursor());
//                                    adapter.reload();
                                }
                            });
                            firstAnimation = false;
                        }
                    }
                }
                mItemIdTopMap.clear();
                return true;
            }
        });

        Handler restoreCleanMapsHandler = new Handler();
        restoreCleanMapsHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mCleanMapImage = true;
                Log.d("HistoryFragment", "Restored maps clean");
            }
        }, 2000);
    }

    @Override
    public String getTitle() {
        return getActivity().getString(R.string.recent);
    }

    class ListAdapter extends CursorAdapter {
        View.OnTouchListener mTouchListener;
        Set<Long> mRequestedLocations = new HashSet<Long>();

        public ListAdapter(Context context, Cursor c) {
            super(context, c, true);

            Log.d("HistoryFragment", "Constructing of ListAdapter");
        }

        @Override
        public long getItemId(int position) {
            Cursor cursor = (Cursor)getItem(position);
            return cursor.getLong(cursor.getColumnIndex(HistoryTable.ID_FIELD));
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

        @Override
        public View newView(Context context, Cursor cursor, ViewGroup parent) {
            View view = getActivity().getLayoutInflater().inflate(R.layout.history_row, null);

            view.setOnTouchListener(HistoryFragment.this);

            if(mMapHeight > 0) {
                RelativeLayout mapPreviewLayout = (RelativeLayout)view.findViewById(R.id.mapPreview);
                mapPreviewLayout.getLayoutParams().height = mMapHeight;
            }

            return view;
        }

        @Override
        public void bindView(final View view, Context context, Cursor cursor) {
            HistoryItem item = HistoryTable.createItem(cursor);

            TextView pinDate = (TextView)view.findViewById(R.id.date);
            pinDate.setText(
                    DateUtils.getRelativeDateTimeString(
                            context,
                            item.getAddDate(),
                            DateUtils.HOUR_IN_MILLIS,
                            DateUtils.WEEK_IN_MILLIS,
                            DateUtils.FORMAT_ABBREV_RELATIVE
                    ).toString().replace(", ", "\n")
            );

            View locationLayout = view.findViewById(R.id.locationLayout);
            if(item.getName() == null || item.getName().isEmpty()) {
                locationLayout.setVisibility(View.GONE);
                requestLocation(item);
            } else {
                TextView name = (TextView)view.findViewById(R.id.locationName);
                name.setText(item.getName());
                locationLayout.setVisibility(View.VISIBLE);
            }

            ImageView mapImage = (ImageView)view.findViewById(R.id.mapImage);

            if(mCleanMapImage) {
                mapImage.setImageResource(R.color.empty_map);
                Log.d("HistoryFragment", "cleaned map image");
            }

            if(mMapHeight == 0) {
                // если размер не известен, то сначала определяем размеры, а потом загружаем карту
                resizeImage(mapImage, view, item);
                Log.d("HistoryFragment", "wait resize");
            } else {
                // иначе карту загружаем сразу
                Log.d("HistoryFragment", "show map at once");
                showMap(mapImage, item);
            }

        }

        public void reload() {
            mRestoreIndex = getListView().getFirstVisiblePosition();
            View v = getListView().getChildAt(0);
            mRestoreTop = (v == null) ? 0 : v.getTop();
            mRestorePosition = true;

            getLoaderManager().restartLoader(LOADER_ID, null, HistoryFragment.this);
        }

        private void resizeImage(final ImageView mapImage, final View view, final HistoryItem item) {
            mapImage.post(new Runnable() {
                @Override
                public void run() {
                    // узнаём ширину
                    mMapWidth = mapImage.getMeasuredWidth();
                    int height = mapImage.getMeasuredHeight();
                    Log.d("HistoryFragment", "map size: " + Integer.toString(mMapWidth) + "x" + Integer.toString(height));

                    // считаем высоту, чтобы стороны карты соответствовали MAP_RATIO
                    ViewGroup.LayoutParams params = mapImage.getLayoutParams();
                    mMapHeight = (int) ((float) mMapWidth * MAP_RATIO);

                    Log.d("HistoryFragment", "new layout height: " + Integer.toString(mMapHeight));

                    // ресайзим
                    RelativeLayout mapPreviewLayout = (RelativeLayout) view.findViewById(R.id.mapPreview);
                    mapPreviewLayout.getLayoutParams().height = mMapHeight;

                    // запрашиваем перерисовку layout
                    mapImage.requestLayout();

                    showMap(mapImage, item);
                }
            });
        }

        protected void showMap(ImageView mapImage, HistoryItem item) {
            try {
                ApplicationInfo ai = mContext.getPackageManager().getApplicationInfo(getActivity().getPackageName(), PackageManager.GET_META_DATA);
                Bundle bundle = ai.metaData;
                String apiKey = bundle.getString("WEB_API_KEY");

                mImageLoader.displayImage(
                        buildMapUrl(item.getLat(), item.getLng(), mMapWidth, mMapHeight, apiKey),
                        mapImage,
                        mDisplayImageOptions
                );
            } catch (PackageManager.NameNotFoundException e) {
                Log.e("HistoryFragment", "web api key is not found");
            }
        }

        protected String buildMapUrl(double lat, double lng, int width, int height, String apiKey) {
            StringBuilder marker = new StringBuilder();
            marker
                    .append(Double.toString(lat))
                    .append(",")
                    .append(Double.toString(lng))
                    .append("|")
                    .append("color:")
                    .append("0x")
                    .append(Integer.toHexString(getResources().getColor(R.color.marker_color)));


            Uri.Builder builder = new Uri.Builder();
            builder
                    .scheme("https")
                    .authority("maps.googleapis.com")
                    .appendPath("maps")
                    .appendPath("api")
                    .appendPath("staticmap")
                    .appendQueryParameter("key", apiKey)
                    .appendQueryParameter("size", Integer.toString(width) + "x" + Integer.toString(height))
                    .appendQueryParameter("sensor", "true")
                    .appendQueryParameter("zoom", Integer.toString(MAP_ZOOM))
                    .appendQueryParameter("markers", marker.toString())
                    .appendQueryParameter("language", Locale.getDefault().getLanguage());

            return builder.toString();
        }

        protected void requestLocation(final HistoryItem item) {
            if(mRequestedLocations.contains(item.getId()))
                return;

            mRequestedLocations.add(item.getId());

            (new GetAddressTask(mContext, new AddressReceiver() {
                @Override
                public void onReceiveAddress(String address) {
                    Db db = new Db(mContext);
                    HistoryTable table = db.getHistoryTable();

                    try {
                        item.setName(address);
                        table.update(item);
                    }
                    finally {
                        db.close();
                    }

                    reload();
                }

                @Override
                public void onError() {
                    Toast toast = Toast.makeText(mContext, R.string.request_address_error, Toast.LENGTH_SHORT);
                    toast.show();
                }

                @Override
                public void onAddressNotFound() {
                    onReceiveAddress(mContext.getString(R.string.address_not_found));
                }

                @Override
                public void onRequestFinish() {
                    mRequestedLocations.remove(item.getId());
                }
            })).execute(new LatLng(item.getLat(), item.getLng()));
        }

        protected void updateItemView(HistoryItem item) {

        }
    }

}
