package me.magicdevel.here;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import me.magicdevel.here.action.FavoriteAction;
import me.magicdevel.here.action.HistoryAction;
import me.magicdevel.here.action.MapAction;

/**
 * Created with IntelliJ IDEA.
 * User: magicdevel
 * Date: 22.06.2014
 * Time: 19:32
 */
public class MainMenuFragment extends ListFragment {
    private static final int MAP_ITEM = 0;
    private static final int RECENT_ITEM = 1;
    private static final int FAVORITES_ITEM = 2;

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        MenuAdapter adapter = new MenuAdapter(getActivity());

        adapter.add(new MenuItem(getString(R.string.map), R.drawable.global, MAP_ITEM));
        adapter.add(new MenuItem(getString(R.string.recent), R.drawable.recents, RECENT_ITEM));
        adapter.add(new MenuItem(getString(R.string.favorites), R.drawable.favorites, FAVORITES_ITEM));

        setListAdapter(adapter);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.main_menu, container, false);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        MenuItem item = (MenuItem)getListAdapter().getItem(position);

        if(item.type == MAP_ITEM)
            ((MapAction)getActivity()).onMapAction();

        switch (item.type) {
            case MAP_ITEM:
                ((MapAction)getActivity()).onMapAction();
                break;
            case RECENT_ITEM:
                ((HistoryAction)getActivity()).onHistoryAction();
                break;
            case FAVORITES_ITEM:
                ((FavoriteAction)getActivity()).onFavoriteAction();
                break;
        }
    }

    private class MenuItem {
        public String tag;
        public int iconRes;
        public int type;

        public MenuItem(String tag, int iconRes, int type) {
            this.tag = tag;
            this.iconRes = iconRes;
            this.type = type;
        }
    }

    public class MenuAdapter extends ArrayAdapter<MenuItem> {

        public MenuAdapter(Context context) {
            super(context, 0);
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.menu_row, null);
            }
            ImageView icon = (ImageView) convertView.findViewById(R.id.menu_row_icon);
            icon.setImageResource(getItem(position).iconRes);
            TextView title = (TextView) convertView.findViewById(R.id.menu_row_title);
            title.setText(getItem(position).tag);

            return convertView;
        }

    }
}