package me.magicdevel.here;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.Toast;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import me.magicdevel.here.action.MapFragmentActions;
import me.magicdevel.here.data.Db;
import me.magicdevel.here.marker.*;
import me.magicdevel.here.parcel.HasParcel;
import me.magicdevel.here.parcel.MapFragmentParcel;
import me.magicdevel.here.route.LoadRouteTask;
import me.magicdevel.here.route.RouteReceiver;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: magicdevel
 * Date: 12.06.2014
 * Time: 13:15
 */
public class MapFragment extends SupportMapFragment implements
        GooglePlayServicesClient.ConnectionCallbacks,
        GooglePlayServicesClient.OnConnectionFailedListener,
        LocationListener,
        GoogleMap.OnMyLocationButtonClickListener,
        GoogleMap.OnMarkerClickListener,
        GoogleMap.OnMarkerDragListener,
        HasParcel,
        RouteReceiver,
        FragmentWithTitle {

    public static final int DEFAULT_ZOOM = 18;
    /**
     * Расстояние в км, начиная с которого вместо линии направления строится маршрут
     */
    public static final double DISTANCE_TO_DRAW_ROUTE = 0.5;
    /**
     * Градусов в километре
     */
    public static final double DEGREE_IN_KM = 1.0 / 111.18;
    /**
     * Расстояние в градусах, начиная с которого, вместо линии направления, строится маршрут
     */
    public static final double DISTANCE_TO_DRAW_ROUTE_IN_DEGREE = DISTANCE_TO_DRAW_ROUTE * DEGREE_IN_KM;
    /**
     * Время в минутах, в течение которого сохранять позицию карты
     */
    public static final long SAVE_POSITION_TIME = 20;
    /**
     * Точность, ниже которой считается безопасным запомнить локацию
     */
    public static final float SAFE_ACCURACY = 15;
    /**
     * Сколько времени в секундах ждать перед запуском автопозиционирования
     */
    public static final long AUTO_POSITION_INTERVAL = 10;

    private LocationClient mLocationClient;
    private Marker mCurrentLocationMarker;
    private Marker mSelectedMarker;
    private Marker mTargetMarker;
    private Location mCurrentLocation;
    private boolean mMapControlsVisible = false;
    private boolean mFirstSelect = false;
    private boolean mFirstAnimation = false;
    private boolean mMapControlsHiding = false;
    private boolean mDraggingMarker = false;
    private Polyline mTargetLine;
    private PolylineOptions mRouteLineOptions;
    private Polyline mRouteLine;
    private MapFragmentParcel mParcel;
    private float mCurrentZoom = 0;
    private LatLng mCurrentCameraPosition = new LatLng(0, 0);
    private LatLng mRouteOrigin = null;
    private LatLng mRouteDestination = null;
    private boolean mNewRoute = true;
    private boolean mFirstRouteShow = true;
    private boolean mPaused = false;
    private boolean mMapInitialized = false;
    /**
     * Время предыдущего завершения фрагмента в милисекундах
     */
    private long mCloseTime = 0;
    /**
     * Время последнего изменения положения камеры в милисекундах
     */
    private long mLastCameraChangeTime = 0;
    /**
     * Автоматическое перемещение камеры за пользователем
     */
    private boolean mAutoMove = false;
    private Animation mFlashingAnimation;

    private ImageButton mFavoriteButton;
    private ImageButton mPinButton;
    private ImageButton mRemoveButton;

    private MarkersCollection mMarkers = new MarkersCollection();

    private static final LocationRequest REQUEST = LocationRequest.create()
            .setInterval(1000)         // 1 second
            .setFastestInterval(16)    // 16ms = 60fps
            .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

    public MapFragment() {
        super();
    }

    @SuppressLint("ValidFragment")
    public MapFragment(MapFragmentParcel parcel) {
        this();

        mParcel = parcel;

        Log.d("map fragment", "setup a parcel");

        if(mParcel == null)
            Log.d("map fragment", "a setup parcel is null");
    }

    public Marker addMarker(Marker marker) {
        mMarkers.add(marker);
        marker.addOnMap(getMap());

        return marker;
    }

    public Marker addMarker(LatLng latLng) {
        Marker marker = isFavoriteLocation(latLng) ? new FavoriteLocationMarker() : new LocationMarker();
        marker.setPosition(latLng);

        addMarker(marker);

        return marker;
    }

    /**
     * Добавляет маркер или возвращает существующий с такими же координатами
     *
     * @param latLng
     * @return
     */
    public Marker addMarkerIf(LatLng latLng) {
        Marker marker = mMarkers.findMarkerByLocation(latLng);

        if(marker == null)
            marker = addMarker(latLng);

        return marker;
    }

    public void removeSelectedMarker() {
        if(mSelectedMarker == null || isSelectedCurrentLocation())
            return;

        if(isSelectedTargetLocation()) {
            deinitTargetMarker();
            mTargetMarker = null;
        }

        mMarkers.remove(mSelectedMarker);

        mSelectedMarker = null;
        hideMapControls();
    }

    public void moveCameraTo(LatLng location, int zoom) {
        mFirstAnimation = true;
        getMap().animateCamera(CameraUpdateFactory.newLatLngZoom(location, zoom));
    }

    protected boolean isFavoriteLocation(LatLng latLng) {
        Db db = new Db(getActivity());

        try {
            return db.getFavoriteTable().hasFavorite(latLng.latitude, latLng.longitude);
        }
        finally {
            db.close();
        }
    }

    private void setUpMapIfNeeded() {
        getMap().setMyLocationEnabled(true);
        getMap().setOnMyLocationButtonClickListener(this);
    }

    private void setUpLocationClientIfNeeded() {
        if (mLocationClient == null) {
            mLocationClient = new LocationClient(
                    getActivity().getApplicationContext(),
                    this,  // ConnectionCallbacks
                    this); // OnConnectionFailedListener
        }
    }

    protected void setCurrentLocationMarker(Location location) {
        if(mCurrentLocationMarker == null) {
            mCurrentLocationMarker = new CurrentLocationMarker();
            mCurrentLocationMarker.setPosition(location);

            addMarker(mCurrentLocationMarker);

//            Toast.makeText(this, "Marker added", Toast.LENGTH_SHORT).show();
        } else {
            mCurrentLocationMarker.setPosition(location);
        }
    }

    protected void prepareControlsToShow() {
        mRemoveButton.setVisibility(isSelectedCurrentLocation() ? View.GONE : View.VISIBLE);
        mPinButton.setVisibility(isSelectedTargetLocation() ? View.GONE : View.VISIBLE);

        warnAboutAccuracy();
        updateFavoriteButton();
    }

    protected void showMapControls() {
        if(mMapControlsVisible) {
            hideMapControls();

            return;
        }

        final View controlsPanel = getActivity().findViewById(R.id.mapControls);

        if(controlsPanel == null)
            return;

        prepareControlsToShow();

        mMapControlsVisible = true;

        controlsPanel.setVisibility(View.VISIBLE);

        Animation bottomUp = AnimationUtils.loadAnimation(getActivity(),
                R.anim.bottom_up);

        bottomUp.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                controlsPanel.bringToFront();
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                controlsPanel.bringToFront();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        controlsPanel.startAnimation(bottomUp);
    }

    protected void hideMapControls() {
        if(!mMapControlsVisible || mMapControlsHiding)
            return;

        final View controlsPanel = getActivity().findViewById(R.id.mapControls);

        if(controlsPanel == null)
            return;

        controlsPanel.findViewById(R.id.mapControls).bringToFront();

        Animation bottomDown = AnimationUtils.loadAnimation(getActivity(),
                R.anim.bottom_down);

        mMapControlsHiding = true;

        bottomDown.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mMapControlsVisible = false;
                mMapControlsHiding = false;

                if (mSelectedMarker != null)
                    showMapControls();
                else
                    controlsPanel.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        controlsPanel.startAnimation(bottomDown);
    }

    public void toggleMarker(Marker marker) {
        if(mSelectedMarker != null && mSelectedMarker.equals(marker)) {

            marker.deselect();
            mSelectedMarker = null;

            hideMapControls();

        } else {

            if(mSelectedMarker != null)
                mSelectedMarker.deselect();

            marker.select();
            mSelectedMarker = marker;
            mFirstSelect = true;

            showMapControls();
        }
    }

    public void toggleMarker(com.google.android.gms.maps.model.Marker marker) {
        Marker m = mMarkers.findByMapMarker(marker);
        if(m != null)
            toggleMarker(m);
    }

    protected boolean isSelectedCurrentLocation() {
        return mSelectedMarker != null
                && mCurrentLocationMarker != null
                && mSelectedMarker.equals(mCurrentLocationMarker);
    }

    protected boolean isSelectedTargetLocation() {
        return mSelectedMarker != null
                && mTargetMarker != null
                && mSelectedMarker.equals(mTargetMarker);
    }

    protected void initTargetMarker() {
        if(mTargetMarker == null) {
            deinitTargetMarker();
            return;
        }

        if(isTooFar() && canShowRoute()) {
//            Log.d("map fragment", "target is too far");
            updateRouteLine();

            mFirstRouteShow = false;
        } else {
//            Log.d("map fragment", "target is near");
            updateTargetLine();
        }
    }

    protected void deinitTargetMarker() {
        removeTargetLine();
        removeRouteLine();
    }

    protected void updateTargetLine() {
        if(mDraggingMarker)
            return;

        addTargetLine();
    }

    protected void updateRouteLine() {
        if(mDraggingMarker)
            return;

        if(mCurrentLocation == null)
            return;

        if(isInNeedOfRouteReloading()) {
            deinitTargetMarker();
            loadRoute();
        } else {
            addRouteLine();
        }
    }

    protected void removeTargetLine() {
        if(mTargetLine == null)
            return;

        mTargetLine.remove();
        mTargetLine = null;
    }

    protected void addTargetLine() {
        deinitTargetMarker();

        if(mTargetMarker == null || mCurrentLocation == null)
            return;

        PolylineOptions line = new PolylineOptions();
        line.add(mCurrentLocationMarker.getPosition(), mTargetMarker.getPosition()).color(0x9fee634c).width(5);

        mTargetLine = getMap().addPolyline(line);
    }

    protected void removeRouteLine() {
        if(mRouteLine == null)
            return;

        mRouteLine.remove();
        mRouteLine = null;
    }

    protected void addRouteLine() {
        if(mRouteLine != null && !mNewRoute)
            return;

        if(mRouteLineOptions != null && getMap() != null) {
            deinitTargetMarker();
            mRouteLine = getMap().addPolyline(mRouteLineOptions);
            mNewRoute = false;
        }
    }

    protected boolean isTooFar() {
        double dstByLat = Math.abs(mTargetMarker.getPosition().latitude - mCurrentLocationMarker.getPosition().latitude);
        double dstByLng = Math.abs(mTargetMarker.getPosition().longitude - mCurrentLocationMarker.getPosition().longitude);

//        Log.d("map fragment", "dst by lat: " + dstByLat);
//        Log.d("map fragment", "dst by lng: " + dstByLng);
//        Log.d("map fragment", "distance to draw route in degree: " + DISTANCE_TO_DRAW_ROUTE_IN_DEGREE);

        return dstByLat >= DISTANCE_TO_DRAW_ROUTE_IN_DEGREE || dstByLng >= DISTANCE_TO_DRAW_ROUTE_IN_DEGREE;
    }

    /**
     * Проверяет, можно ли показать маршрут.
     * После первого показа всегда возвращает true.
     * Перед первым показом проверяет, на сколько точно определено положение.
     *
     * @return
     */
    protected boolean canShowRoute()
    {
        if(!mFirstRouteShow)
            return true;

        return mCurrentLocation != null && mCurrentLocation.hasAccuracy() && mCurrentLocation.getAccuracy() <= SAFE_ACCURACY;
    }

    protected boolean isInNeedOfRouteReloading() {
        if(mRouteDestination == null || mRouteOrigin == null)
            return true;

        if(!mRouteDestination.equals(mTargetMarker.getPosition()))
            return true;

        return false;
    }

    protected void initControls() {
        initFavoriteButton();
        initPinButton();
        initRemoveButton();
    }

    protected void initFavoriteButton() {
        mFavoriteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickFavoriteButton();
            }
        });
    }

    protected void initPinButton() {
        mPinButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickPinButton();
            }
        });
    }

    protected void initRemoveButton() {
        mRemoveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickRemoveButton();
            }
        });
    }

    protected void applyParcel() {
        if(mParcel == null) {
            Log.d("map fragment", "A parcel is null");

            return;
        }

        Log.d("map fragment", "applying of a parcel");

        mCloseTime = mParcel.getSaveTime();

        applyParcelForCameraPosition();
        applyParcelForMarkers();

        mFirstSelect = false;
    }

    public void applyParcel(Parcelable parcel) {
        mParcel = (MapFragmentParcel)parcel;

        if(getMap() != null)
            applyParcel();
    }

    protected void applyParcelForCameraPosition() {
        mCurrentZoom = mParcel.getZoom();
        mCurrentCameraPosition = mParcel.getCameraPosition();

        getMap().moveCamera(CameraUpdateFactory.newLatLngZoom(
                mParcel.getCameraPosition(),
                mParcel.getZoom()
        ));
    }

    protected void applyParcelForMarkers() {
        for (Marker marker : mParcel.getMarkers()) {
            addMarker(marker);

            if(marker instanceof CurrentLocationMarker)
                mCurrentLocationMarker = marker;
            else if(marker instanceof TargetLocationMarker)
                mTargetMarker = marker;

            if(marker.isSelected()) {
                toggleMarker(marker);
            }
        }
    }

    protected void updateFavoriteButton() {
        Db db = new Db(getActivity());

        try {
            LatLng selectedPosition = mSelectedMarker.getPosition();
            boolean hasFavorite = db.getFavoriteTable().hasFavorite(selectedPosition.latitude, selectedPosition.longitude);
            Log.d("map fragment", "Marker is favorite: " + hasFavorite);
            mFavoriteButton.setImageResource(hasFavorite ? R.drawable.favorites_active : R.drawable.favorites);
        }
        finally {
            db.close();
        }
    }

    protected void warnAboutAccuracy() {
        if(!isSelectedCurrentLocation()) {
            stopWarnAboutAccuracy();
            return;
        }

        if(mCurrentLocation == null || !mCurrentLocation.hasAccuracy() || mCurrentLocation.getAccuracy() > SAFE_ACCURACY) {
            if(mPinButton.getAnimation() == null)
                mPinButton.startAnimation(mFlashingAnimation);
        } else {
            stopWarnAboutAccuracy();
        }
    }

    protected void stopWarnAboutAccuracy() {
        mPinButton.clearAnimation();
    }

    /**
     * Проверяет, нужно ли сохранить позицию карты после загрузки фрагмента
     *
     * @return true - нужно сохранить позицию
     */
    protected boolean isPositionSaved() {
        Calendar c = Calendar.getInstance();

        Log.d("map fragment", "Current time: " + c.getTimeInMillis());
        Log.d("map fragment", "Save time: " + mCloseTime);

        return (c.getTimeInMillis() - mCloseTime) / 1000 / 60 < SAVE_POSITION_TIME;
    }

    protected void doPin() {
        TargetLocationMarker marker = new TargetLocationMarker();
        marker.setPosition(mSelectedMarker.getPosition());

        addMarker(marker);

        if(!isSelectedCurrentLocation()) {
            removeSelectedMarker();
        }
        toggleMarker(marker);

        if(mTargetMarker != null) {
            deinitTargetMarker();
            mMarkers.remove(mTargetMarker);
        }

        mTargetMarker = marker;

        initTargetMarker();

        if(getActivity() instanceof MapFragmentActions) {
            long id = ((MapFragmentActions) getActivity()).onAddTarget(mTargetMarker.getPosition().latitude, mTargetMarker.getPosition().longitude);
            Log.v("MapFragment", "Added target with id - " + Long.toString(id));

            marker.setHistoryId(id);
        }
    }

    protected boolean canDoAutoMove() {
        if(!isSelectedCurrentLocation())
            return false;

        if(mAutoMove)
            return true;

        Calendar c = Calendar.getInstance();
        return (c.getTimeInMillis() - mLastCameraChangeTime) / 1000 > AUTO_POSITION_INTERVAL;
    }

    public void runAfterMapInit(InitMapListener listener) {
        if(mMapInitialized && getMap() != null) {
            listener.onInitMap(this, getMap());
        } else {
            new InitMapHandler().post(listener);
        }
    }

    @Override
    public Parcelable getParcel() {
        MapFragmentParcel parcel = new MapFragmentParcel();

        parcel.saveTime();
        parcel.setCameraPosition(mCurrentCameraPosition);
        parcel.setZoom(mCurrentZoom);
        parcel.setMarkers(mMarkers);

        return parcel;
    }

    @Override
    public String getParcelName() {
        return "map";
    }

    protected void loadRoute() {
        mRouteOrigin = mCurrentLocationMarker.getPosition();
        mRouteDestination = mTargetMarker.getPosition();

        LoadRouteTask loadRouteTask = new LoadRouteTask(this, mRouteOrigin, mRouteDestination);
        loadRouteTask.execute();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d("map fragment", "creating view");

        MarkerBitmapFactory.instance().initialize(getResources());

        View mapView = super.onCreateView(inflater, container, savedInstanceState);

        inflater.inflate(R.layout.map_panel, container);
        container.findViewById(R.id.mapControls).setVisibility(View.GONE);

        mFavoriteButton = (ImageButton)container.findViewById(R.id.mapFavoriteButton);
        mPinButton = (ImageButton)container.findViewById(R.id.mapPinButton);
        mRemoveButton = (ImageButton)container.findViewById(R.id.mapRemoveButton);

        mFlashingAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.flashing);

        return mapView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mPaused = false;

        new LoadMapHandler().post(new LoadMapListener() {
            @Override
            public void onMapLoaded() {
                initControls();

                applyParcel();

                getMap().getUiSettings().setZoomControlsEnabled(false);

                getMap().setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
                    @Override
                    public void onMapLongClick(LatLng latLng) {
                        Marker marker = addMarker(latLng);
                        toggleMarker(marker);
                    }
                });

                getMap().setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
                    @Override
                    public void onCameraChange(CameraPosition cameraPosition) {
                        mCurrentZoom = cameraPosition.zoom;
                        mCurrentCameraPosition = cameraPosition.target;
                        Calendar c = Calendar.getInstance();
                        mLastCameraChangeTime = c.getTimeInMillis();
                        Log.d("map fragment", "Camera changed");
                    }
                });
            }
        });
    }

    @Override
    public void onDestroyView() {
        View controls = getActivity().findViewById(R.id.mapControls);

        ((ViewGroup)(controls.getParent())).removeView(controls);

        super.onDestroyView();
    }

    @Override
    public void onResume() {
        super.onResume();

        mPaused = false;

        new LoadMapHandler().post(new LoadMapListener() {
            @Override
            public void onMapLoaded() {
                setUpMapIfNeeded();
                setUpLocationClientIfNeeded();
                mLocationClient.connect();

                getMap().setOnMarkerClickListener(MapFragment.this);
                getMap().setOnMarkerDragListener(MapFragment.this);

                mMapInitialized = true;
            }
        });
    }

    @Override
    public void onPause() {
        mPaused = true;
        mMapInitialized = false;

        if(getMap() != null) {
            getMap().setOnMarkerClickListener(null);
            getMap().setOnMarkerDragListener(null);
        }

        stopWarnAboutAccuracy();

        super.onPause();

        if(mLocationClient != null)
            mLocationClient.disconnect();
    }

    /**
     * Implementation of {@link LocationListener}.
     */
    @Override
    public void onLocationChanged(Location location) {
        if(getMap() == null)
            return;

        setCurrentLocationMarker(location);
        mCurrentLocation = location;

        initTargetMarker();

        if(isSelectedCurrentLocation()) {
            updateFavoriteButton();
            warnAboutAccuracy();
        }

        if(!mFirstAnimation) {
            mFirstAnimation = true;

            if(!isPositionSaved()) {
                getMap().animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), DEFAULT_ZOOM), new GoogleMap.CancelableCallback() {
                    @Override
                    public void onFinish() {
                        if (!mFirstSelect) {
                            if(!isSelectedCurrentLocation())
                                toggleMarker(mCurrentLocationMarker);

                            mAutoMove = true;
                        }
                        Log.d("map fragment", "camera animation finished");
                    }

                    @Override
                    public void onCancel() {

                    }
                });
            }
        } else if(canDoAutoMove()) {
            getMap().animateCamera(CameraUpdateFactory.newLatLng(new LatLng(location.getLatitude(), location.getLongitude())), new GoogleMap.CancelableCallback() {
                @Override
                public void onFinish() {
                    mAutoMove = true;
                    Log.d("map fragment", "continue automoving");
                }

                @Override
                public void onCancel() {
                    mAutoMove = false;
                    Log.d("map fragment", "cancel automoving");
                }
            });
        }
//        mMessageView.setText("Location = " + location);
    }

    /**
     * Callback called when connected to GCore. Implementation of {@link com.google.android.gms.common.GooglePlayServicesClient.ConnectionCallbacks}.
     */
    @Override
    public void onConnected(Bundle connectionHint) {
        mLocationClient.requestLocationUpdates(
                REQUEST,
                this);  // LocationListener
    }

    /**
     * Callback called when disconnected from GCore. Implementation of {@link com.google.android.gms.common.GooglePlayServicesClient.ConnectionCallbacks}.
     */
    @Override
    public void onDisconnected() {
        // Do nothing
    }

    /**
     * Implementation of {@link com.google.android.gms.common.GooglePlayServicesClient.OnConnectionFailedListener}.
     */
    @Override
    public void onConnectionFailed(ConnectionResult result) {
        // Do nothing
    }

    @Override
    public boolean onMyLocationButtonClick() {
//        Toast.makeText(getActivity(), "MyLocation button clicked", Toast.LENGTH_SHORT).show();
        // Return false so that we don't consume the event and the default behavior still occurs
        // (the camera animates to the user's current position).
        return false;
    }

    @Override
    public boolean onMarkerClick(com.google.android.gms.maps.model.Marker marker) {
        toggleMarker(marker);

        return true;
    }

    @Override
    public void onMarkerDragStart(com.google.android.gms.maps.model.Marker marker) {
        deinitTargetMarker();
        mDraggingMarker = true;
    }

    @Override
    public void onMarkerDrag(com.google.android.gms.maps.model.Marker marker) {
    }

    @Override
    public void onMarkerDragEnd(com.google.android.gms.maps.model.Marker mapMarker) {
        mDraggingMarker = false;

        Marker marker = mMarkers.findByMapMarker(mapMarker);
        if(marker != null) {
            boolean isFavoriteLocation = isFavoriteLocation(marker.getPosition());
            if((isFavoriteLocation && marker instanceof LocationMarker) || (!isFavoriteLocation && marker instanceof FavoriteLocationMarker)) {
                // если это избранная локация, а маркер - обычный
                // или
                // если локация - не избранная, а маркер - избранный
                // то
                // пересоздаём маркер (он станет нужного вида)
                mMarkers.remove(marker);
                marker = addMarker(mapMarker.getPosition());
                mSelectedMarker = null;
            }
        }

        if(mCurrentLocationMarker != null && mCurrentLocationMarker.isMarkerOwner(mapMarker)) {
            onCurrentLocationMarkerDragEnd();
        } else if(mSelectedMarker == null || !mSelectedMarker.isMarkerOwner(mapMarker)) {
            if(marker != null)
                toggleMarker(marker);
        }

        if(mTargetMarker != null && mTargetMarker.isMarkerOwner(mapMarker)) {
            onTargetMarkerDragEnd();
        }

        initTargetMarker();
    }

    protected void onCurrentLocationMarkerDragEnd() {
        Marker marker = new LocationMarker();
        marker.setPosition(mCurrentLocationMarker.getPosition());

        addMarker(marker);
        toggleMarker(marker);

        mCurrentLocationMarker.setPosition(mCurrentLocation);
    }

    protected void onTargetMarkerDragEnd() {
        if(getActivity() instanceof MapFragmentActions) {
            ((MapFragmentActions)getActivity()).onUpdateTarget(
                ((TargetLocationMarker)mTargetMarker).getHistoryId(),
                mTargetMarker.getPosition().latitude,
                mTargetMarker.getPosition().longitude
            );

            Log.v("MapFragment", "Updated history item - " + Long.toString(((TargetLocationMarker)mTargetMarker).getHistoryId()));
        }
    }

    protected void onClickFavoriteButton() {
        if(mSelectedMarker == null)
            return;

        if(getActivity() instanceof MapFragmentActions) {
            long id = ((MapFragmentActions) getActivity()).onAddFavorite(mSelectedMarker.getPosition().latitude, mSelectedMarker.getPosition().longitude);
            Log.v("MapFragment", "Added favorite with id - " + Long.toString(id));
        }

        // передобавляем маркер; он сменит вид на вид избранного
        if(mSelectedMarker instanceof LocationMarker) {
            LatLng position = mSelectedMarker.getPosition();
            mMarkers.remove(mSelectedMarker);
            mSelectedMarker = null;

            toggleMarker(addMarker(position));
        } else {
            prepareControlsToShow();
        }

        Toast.makeText(getActivity(), R.string.added_favorite, Toast.LENGTH_SHORT).show();
    }

    protected void onClickPinButton() {
        if(mSelectedMarker == null)
            return;

        DialogInterface.OnClickListener questionHandler = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(which == DialogInterface.BUTTON_POSITIVE)
                    doPin();
            }
        };

        if(isSelectedCurrentLocation() && (mCurrentLocation == null || !mCurrentLocation.hasAccuracy() || mCurrentLocation.getAccuracy() > SAFE_ACCURACY)) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setMessage(R.string.warn_about_accuracy)
                    .setPositiveButton(R.string.force_add, questionHandler)
                    .setNegativeButton(R.string.wait_accuracy, questionHandler)
                    .show();

        } else {
            doPin();
        }
    }

    protected void onClickRemoveButton() {
        if(!isSelectedCurrentLocation())
            removeSelectedMarker();
    }

    @Override
    public void onReceiveRoute(LatLng origin, LatLng destination, List<List<HashMap<String, String>>> route) {
        if(!destination.equals(mRouteDestination))
            return;

        mRouteLineOptions = new PolylineOptions();
        ArrayList<LatLng> points = null;

        // traversing through routes
        for (int i = 0; i < route.size(); i++) {
            points = new ArrayList<LatLng>();
            List<HashMap<String, String>> path = route.get(i);

            for (int j = 0; j < path.size(); j++) {
                HashMap<String, String> point = path.get(j);

                double lat = Double.parseDouble(point.get("lat"));
                double lng = Double.parseDouble(point.get("lng"));
                LatLng position = new LatLng(lat, lng);

                points.add(position);
            }

            mRouteLineOptions.addAll(points);
            mRouteLineOptions.width(5);
            mRouteLineOptions.color(0x9fee634c);
        }

        mNewRoute = true;
        initTargetMarker();
    }

    @Override
    public String getTitle() {
        return getActivity().getString(R.string.map);
    }

    interface LoadMapListener {
        void onMapLoaded();
    }

    class LoadMapHandler {
        void post(final LoadMapListener listener) {
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if(getMap() == null) {
                        if(!mPaused)
                            post(listener);
                    } else {
                        listener.onMapLoaded();
                    }
                }
            }, 250);
        }
    }

    interface InitMapListener {
        void onInitMap(MapFragment fragment, GoogleMap map);
    }

    class InitMapHandler {
        void post(final InitMapListener listener) {
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if(!mMapInitialized) {
                        if(!mPaused)
                            post(listener);
                    } else {
                        if(getMap() != null)
                            listener.onInitMap(MapFragment.this, getMap());
                    }
                }
            }, 250);
        }
    }
}
