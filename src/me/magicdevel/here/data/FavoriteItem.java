package me.magicdevel.here.data;

/**
 * Created by sergejdorodnyh on 21.10.14.
 */
public class FavoriteItem extends LocationItem {
    public FavoriteItem(long id, double lat, double lng, String name, long addDate) {
        super(id, lat, lng, name, addDate);
    }
}
