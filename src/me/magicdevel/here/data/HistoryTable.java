package me.magicdevel.here.data;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;
import me.magicdevel.here.data.exception.NotFound;

/**
 * Created by magicdevel on 23.07.2014.
 */
public class HistoryTable {
    public static final String TABLE_NAME = "history";

    public static final String ID_FIELD = "_id";
    public static final String LAT_FIELD = "lat";
    public static final String LNG_FIELD = "lng";
    public static final String NAME_FIELD = "name";
    public static final String ADD_DATE = "add_date";

    private final SQLiteDatabase mDb;

    public HistoryTable(SQLiteDatabase db) {
        mDb = db;
    }

    public long insert(double lat, double lng, String name) {
        try {

            ContentValues values = new ContentValues();
            values.put(LAT_FIELD, lat);
            values.put(LNG_FIELD, lng);
            values.put(ADD_DATE, java.lang.System.currentTimeMillis());
            if(name != null)
                values.put(NAME_FIELD, name);

            return mDb.insert(TABLE_NAME, null, values);

        } catch (SQLiteException e) {

            Log.e("HistoryTable", "insert error", e);

            return -1;
        }
    }

    public void update(HistoryItem item) {
        ContentValues values = new ContentValues();
        values.put(LAT_FIELD, item.getLat());
        values.put(LNG_FIELD, item.getLng());
        values.put(ADD_DATE, item.getAddDate());
        values.put(NAME_FIELD, item.getName());

        mDb.update(TABLE_NAME, values, ID_FIELD + "=?", new String[] { Long.toString(item.getId()) });
    }

    public void delete(long id) {
        mDb.delete(TABLE_NAME, ID_FIELD + "=?", new String[] { Long.toString(id) });
    }

    public Cursor findAll() {
        return mDb.query(TABLE_NAME, null, null, null, null, null, ID_FIELD + " desc");
    }

    /**
     * Поиск по id
     *
     * @param id
     * @return
     * @throws me.magicdevel.here.data.exception.NotFound
     */
    public HistoryItem find(long id) throws NotFound {
        Cursor cursor = mDb.query(TABLE_NAME, null, ID_FIELD + "=" + id, null, null, null, null);

        try {
            if (cursor.getCount() == 0)
                throw new NotFound();

            cursor.moveToFirst();

            return createItem(cursor);
        }
        finally {
            cursor.close();
        }
    }

    public static final String getCreateQuery() {
        return "create table " + TABLE_NAME + " (" +
                ID_FIELD + " integer primary key autoincrement, " +
                NAME_FIELD + " text, " +
                LAT_FIELD + " real not null, " +
                LNG_FIELD + " real not null, " +
                ADD_DATE + " int not null" +
                ");";
    }

    public static HistoryItem createItem(Cursor c) {
        return new HistoryItem(
                c.getLong(c.getColumnIndex(ID_FIELD)),
                c.getDouble(c.getColumnIndex(LAT_FIELD)),
                c.getDouble(c.getColumnIndex(LNG_FIELD)),
                c.getString(c.getColumnIndex(NAME_FIELD)),
                c.getLong(c.getColumnIndex(ADD_DATE))
        );
    }
}
