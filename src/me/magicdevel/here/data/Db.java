package me.magicdevel.here.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;

/**
 * Created by magicdevel on 23.07.2014.
 */
public class Db {
    public static final String DB_NAME = "here";
    public static final int DB_VERSION = 1;

    private SQLiteDatabase mDb;
    private final Context mContext;
    private final DbHelper mDbHelper;

    public Db(Context c) {
        mContext = c;
        mDbHelper = new DbHelper(c, DB_NAME, null, DB_VERSION);
    }

    public void close() {
        if(mDb == null)
            return;

        mDb.close();
        mDb = null;
    }

    public void open() throws SQLiteException {
        if(mDb != null)
            return;

        try {
            mDb = mDbHelper.getWritableDatabase();
        } catch (SQLiteException e) {
            Log.v("Db", "error of opening writable database", e);
            mDb = mDbHelper.getReadableDatabase();
        }
    }

    public HistoryTable getHistoryTable() {
        open();

        return new HistoryTable(mDb);
    }

    public FavoriteTable getFavoriteTable() {
        open();

        return new FavoriteTable(mDb);
    }
}
