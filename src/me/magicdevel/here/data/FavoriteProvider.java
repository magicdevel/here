package me.magicdevel.here.data;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;

/**
 * Created by sergejdorodnyh on 21.10.14.
 */
public class FavoriteProvider extends ContentProvider {
    public static final String PROVIDER_NAME = "me.magicdevel.here.favorite";

    /** A uri to do operations on cust_master table. A content provider is identified by its uri */
    public static final Uri CONTENT_URI = Uri.parse("content://" + PROVIDER_NAME + "/favorite" );

    /** Constants to identify the requested operation */
    private static final int FAVORITE = 1;

    private static final UriMatcher mUriMatcher;
    static {
        mUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        mUriMatcher.addURI(PROVIDER_NAME, "favorite", FAVORITE);
    }

    Db mDb;

    @Override
    public boolean onCreate() {
        mDb = new Db(getContext());

        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        if(mUriMatcher.match(uri) == FAVORITE) {
            Cursor cursor = mDb.getFavoriteTable().findAll();
            return cursor;
        } else {
            return null;
        }
    }

    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        return null;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        return 0;
    }
}
