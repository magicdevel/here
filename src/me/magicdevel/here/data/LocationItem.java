package me.magicdevel.here.data;

/**
 * Базовый класс для моделей локации
 */
public class LocationItem {
    private long mId;
    private double mLat;
    private double mLng;
    private String mName;
    private long mAddDate;

    protected LocationItem(long id, double lat, double lng, String name, long addDate) {
        mId = id;
        mLat = lat;
        mLng = lng;
        mName = name;
        mAddDate = addDate;
    }

    public long getId() {
        return mId;
    }

    public void setId(long id) {
        mId = id;
    }

    public double getLat() {
        return mLat;
    }

    public void setLat(double lat) {
        mLat = lat;
    }

    public double getLng() {
        return mLng;
    }

    public void setLng(double lng) {
        mLng = lng;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public long getAddDate() {
        return mAddDate;
    }

    public void setAddDate(long addDate) {
        mAddDate = addDate;
    }
}
