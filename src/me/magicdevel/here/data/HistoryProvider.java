package me.magicdevel.here.data;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;

/**
 * Created by magicdevel on 24.07.2014.
 */
public class HistoryProvider extends ContentProvider {
    public static final String PROVIDER_NAME = "me.magicdevel.here.history";

    /** A uri to do operations on cust_master table. A content provider is identified by its uri */
    public static final Uri CONTENT_URI = Uri.parse("content://" + PROVIDER_NAME + "/history" );

    /** Constants to identify the requested operation */
    private static final int HISTORY = 1;

    private static final UriMatcher mUriMatcher;
    static {
        mUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        mUriMatcher.addURI(PROVIDER_NAME, "history", HISTORY);
    }

    Db mDb;

    @Override
    public boolean onCreate() {
        mDb = new Db(getContext());

        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        if(mUriMatcher.match(uri) == HISTORY) {
            Cursor cursor = mDb.getHistoryTable().findAll();
            return cursor;
        } else {
            return null;
        }
    }

    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        return null;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        return 0;
    }
}
