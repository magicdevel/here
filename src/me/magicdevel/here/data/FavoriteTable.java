package me.magicdevel.here.data;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;
import me.magicdevel.here.data.exception.NotFound;

/**
 * Created by sergejdorodnyh on 21.10.14.
 */
public class FavoriteTable {
    public static final String TABLE_NAME = "favorite";

    public static final String ID_FIELD = "_id";
    public static final String LAT_FIELD = "lat";
    public static final String LNG_FIELD = "lng";
    public static final String NAME_FIELD = "name";
    public static final String ADD_DATE = "add_date";

    private final SQLiteDatabase mDb;

    public FavoriteTable(SQLiteDatabase db) {
        mDb = db;
    }

    public long insert(double lat, double lng, String name) {
        try {

            ContentValues values = new ContentValues();
            values.put(LAT_FIELD, Double.toString(lat));
            values.put(LNG_FIELD, Double.toString(lng));
            values.put(ADD_DATE, java.lang.System.currentTimeMillis());
            if(name != null)
                values.put(NAME_FIELD, name);

            return mDb.insert(TABLE_NAME, null, values);

        } catch (SQLiteException e) {

            Log.e("FavoriteTable", "insert error", e);

            return -1;
        }
    }

    public void update(FavoriteItem item) {
        ContentValues values = new ContentValues();
        values.put(LAT_FIELD, Double.toString(item.getLat()));
        values.put(LNG_FIELD, Double.toString(item.getLng()));
        values.put(ADD_DATE, item.getAddDate());
        values.put(NAME_FIELD, item.getName());

        mDb.update(TABLE_NAME, values, ID_FIELD + "=?", new String[] { Long.toString(item.getId()) });
    }

    public void delete(long id) {
        mDb.delete(TABLE_NAME, ID_FIELD + "=?", new String[] { Long.toString(id) });
    }

    public Cursor findAll() {
        return mDb.query(TABLE_NAME, null, null, null, null, null, ID_FIELD + " desc");
    }

    /**
     * Поиск по id
     *
     * @param id
     * @return
     * @throws me.magicdevel.here.data.exception.NotFound
     */
    public FavoriteItem find(long id) throws NotFound {
        Cursor cursor = mDb.query(TABLE_NAME, null, ID_FIELD + "=" + id, null, null, null, null);
        try {
            if (cursor.getCount() == 0)
                throw new NotFound();

            cursor.moveToFirst();

            return createItem(cursor);
        }
        finally {
            cursor.close();
        }
    }

    public boolean hasFavorite(double lat, double lng) {
        return DatabaseUtils.queryNumEntries(mDb, TABLE_NAME, LAT_FIELD + "=? and " + LNG_FIELD + "=?", new String[] { Double.toString(lat), Double.toString(lng) }) > 0;
    }

    /**
     * Поиск по координатам
     *
     * @param lat
     * @param lng
     * @return
     * @throws NotFound
     */
    public FavoriteItem findByLatLng(double lat, double lng) throws NotFound {
        Cursor cursor = mDb.query(TABLE_NAME, null, LAT_FIELD + "=? and " + LNG_FIELD + "=?", new String[] { Double.toString(lat), Double.toString(lng) }, null, null, null);
        try {
            if (cursor.getCount() == 0)
                throw new NotFound();

            cursor.moveToFirst();

            return createItem(cursor);
        }
        finally {
            cursor.close();
        }
    }

    public static final String getCreateQuery() {
        // храним координаты в строках для более точного поиска
        return "create table " + TABLE_NAME + " (" +
                ID_FIELD + " integer primary key autoincrement, " +
                NAME_FIELD + " text, " +
                LAT_FIELD + " text not null, " +
                LNG_FIELD + " text not null, " +
                ADD_DATE + " int not null" +
                ");";
    }

    public static final String[] getCreateIndexesQueries() {
        return new String[] {
          "create index favorite_location_idx on " + TABLE_NAME + "(" + LAT_FIELD + ", " + LNG_FIELD + ");"
        };
    }

    public static FavoriteItem createItem(Cursor c) {
        return new FavoriteItem(
                c.getLong(c.getColumnIndex(ID_FIELD)),
                Double.parseDouble(c.getString(c.getColumnIndex(LAT_FIELD))),
                Double.parseDouble(c.getString(c.getColumnIndex(LNG_FIELD))),
                c.getString(c.getColumnIndex(NAME_FIELD)),
                c.getLong(c.getColumnIndex(ADD_DATE))
        );
    }
}
