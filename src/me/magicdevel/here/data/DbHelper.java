package me.magicdevel.here.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by magicdevel on 23.07.2014.
 */
public class DbHelper extends SQLiteOpenHelper {
    public DbHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.v("DbHelper", "Creating tables");

        try {
            db.execSQL(HistoryTable.getCreateQuery());
            db.execSQL(FavoriteTable.getCreateQuery());

            for(String indexQuery : FavoriteTable.getCreateIndexesQueries())
                db.execSQL(indexQuery);

        } catch (SQLiteException e) {
            Log.e("DbHelper", "creating tables error", e);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
