package me.magicdevel.here.data;

/**
 * Created by magicdevel on 24.07.2014.
 */
public class HistoryItem extends LocationItem {
    public HistoryItem(long id, double lat, double lng, String name, long addDate) {
        super(id, lat, lng, name, addDate);
    }
}
