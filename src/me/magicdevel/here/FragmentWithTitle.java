package me.magicdevel.here;

/**
 * Created by sergejdorodnyh on 08.11.14.
 */
public interface FragmentWithTitle {
    String getTitle();
}
