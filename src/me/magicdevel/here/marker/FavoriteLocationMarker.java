package me.magicdevel.here.marker;

import android.graphics.Bitmap;

/**
 * Created by sergejdorodnyh on 01.11.14.
 */
public class FavoriteLocationMarker extends Marker {
    @Override
    protected Bitmap createMarkerBitmap() {
        return MarkerBitmapFactory.instance().getFavoriteLocation();
    }

    @Override
    protected Bitmap createSelectedMarkerBitmap() {
        return MarkerBitmapFactory.instance().getSelectedFavoriteLocation();
    }
}
