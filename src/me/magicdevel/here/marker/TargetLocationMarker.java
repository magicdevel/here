package me.magicdevel.here.marker;

import android.graphics.Bitmap;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * Created by magicdevel on 18.07.2014.
 */
public class TargetLocationMarker extends Marker {
    private long mHistoryId = 0;

    public void setHistoryId(long id) {
        mHistoryId = id;
    }

    public long getHistoryId() {
        return mHistoryId;
    }

    @Override
    protected void initOptions(MarkerOptions options) {
        super.initOptions(options);

        // размер картинки - 63х82
        // смещение острия: снизу - 5, слева - 30
//        options.anchor((63.0f - 5.0f) / 63.0f, 24.0f / 82.0f);
        options.anchor(30.0f / 82.0f, (63.0f - 5.0f) / 63.0f);
    }

    @Override
    protected Bitmap createMarkerBitmap() {
        return MarkerBitmapFactory.instance().getTargetLocation();
    }

    @Override
    protected Bitmap createSelectedMarkerBitmap() {
        return MarkerBitmapFactory.instance().getSelectedTargetLocation();
    }
}
