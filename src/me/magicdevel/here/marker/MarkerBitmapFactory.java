package me.magicdevel.here.marker;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import me.magicdevel.here.R;

/**
 * Factory to make marker bitmaps
 */
public class MarkerBitmapFactory {
    private static MarkerBitmapFactory mInstance;

    private Bitmap mCurrentLocation;
    private Bitmap mFavoriteLocation;
    private Bitmap mLocation;
    private Bitmap mTargetLocation;
    private Bitmap mSelectedCurrentLocation;
    private Bitmap mSelectedFavoriteLocation;
    private Bitmap mSelectedLocation;
    private Bitmap mSelectedTargetLocation;
    private Bitmap mSelection;
    private Bitmap mBigSelection;

    private Resources mResources;

    private MarkerBitmapFactory() {

    }

    public void initialize(Resources resources) {
        mResources = resources;
    }

    public Bitmap getCurrentLocation() {
        if(mCurrentLocation == null)
        {
            mCurrentLocation = BitmapFactory.decodeResource(mResources, R.drawable.current_location_pin);
        }
        return mCurrentLocation;
    }

    public Bitmap getSelectedCurrentLocation() {
        if(mSelectedCurrentLocation == null) {
            Bitmap marker = getCurrentLocation();
            mSelectedCurrentLocation = Bitmap.createBitmap(marker.getWidth(), marker.getHeight(), Bitmap.Config.ARGB_8888);

            Canvas canvas = new Canvas(mSelectedCurrentLocation);
            canvas.drawBitmap(marker, 0, 0, null);
            canvas.drawBitmap(getSelection(), 0, 0, null);
        }
        return mSelectedCurrentLocation;
    }

    public Bitmap getLocation() {
        if(mLocation == null)
        {
            mLocation = BitmapFactory.decodeResource(mResources, R.drawable.pin);
        }
        return mLocation;
    }

    public Bitmap getSelectedLocation() {
        if(mSelectedLocation == null) {
            Bitmap marker = getLocation();
            mSelectedLocation = Bitmap.createBitmap(marker.getWidth(), marker.getHeight(), Bitmap.Config.ARGB_8888);

            Canvas canvas = new Canvas(mSelectedLocation);
            canvas.drawBitmap(marker, 0, 0, null);
            canvas.drawBitmap(getSelection(), 0, 0, null);
        }
        return mSelectedLocation;
    }

    public Bitmap getFavoriteLocation() {
        if(mFavoriteLocation == null)
        {
            mFavoriteLocation = BitmapFactory.decodeResource(mResources, R.drawable.favorite_pin);
        }
        return mFavoriteLocation;
    }

    public Bitmap getSelectedFavoriteLocation() {
        if(mSelectedFavoriteLocation == null) {
            Bitmap marker = getFavoriteLocation();
            mSelectedFavoriteLocation = Bitmap.createBitmap(marker.getWidth(), marker.getHeight(), Bitmap.Config.ARGB_8888);

            Canvas canvas = new Canvas(mSelectedFavoriteLocation);
            canvas.drawBitmap(marker, 0, 0, null);
            canvas.drawBitmap(getSelection(), 0, 0, null);
        }
        return mSelectedFavoriteLocation;
    }

    public Bitmap getTargetLocation() {
        if(mTargetLocation == null)
        {
            mTargetLocation = BitmapFactory.decodeResource(mResources, R.drawable.target_pin);
        }
        return mTargetLocation;
    }

    public Bitmap getSelectedTargetLocation() {
        if(mSelectedTargetLocation == null) {
            Bitmap marker = getTargetLocation();
            mSelectedTargetLocation = Bitmap.createBitmap(marker.getWidth(), marker.getHeight(), Bitmap.Config.ARGB_8888);

            Canvas canvas = new Canvas(mSelectedTargetLocation);
            canvas.drawBitmap(marker, 0, 0, null);
            canvas.drawBitmap(getBigSelection(), 0, 0, null);
        }
        return mSelectedTargetLocation;
    }

    private Bitmap getSelection() {
        if(mSelection == null) {
            mSelection = BitmapFactory.decodeResource(mResources, R.drawable.pin_selection);
        }
        return mSelection;
    }

    private Bitmap getBigSelection() {
        if(mBigSelection == null) {
            mBigSelection = BitmapFactory.decodeResource(mResources, R.drawable.target_pin_selection);
        }
        return mBigSelection;
    }

    public static MarkerBitmapFactory instance() {
        if(mInstance == null) {
            mInstance = new MarkerBitmapFactory();
        }
        return mInstance;
    }
}
