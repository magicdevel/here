package me.magicdevel.here.marker;

import android.graphics.Bitmap;
import android.location.Location;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * Created by magicdevel on 15.07.2014.
 */
abstract public class Marker {
    private Bitmap mMarkerBitmap;
    private Bitmap mSelectedMarkerBitmap;

    private MarkerOptions mMarkerOptions = new MarkerOptions();
    private com.google.android.gms.maps.model.Marker mMarker;

    private boolean mSelected = false;

    public Marker() {
        initOptions(mMarkerOptions);
    }

    protected void initOptions(MarkerOptions options) {
        options.draggable(true);
    }

    public void addOnMap(GoogleMap map) {
        setSelectState(mSelected);

        mMarker = map.addMarker(mMarkerOptions);
    }

    public void removeFromMap() {
        if(mMarker != null)
            mMarker.remove();
    }

    public void setPosition(Location location) {
        if(mMarker == null)
            mMarkerOptions.position(new LatLng(location.getLatitude(), location.getLongitude()));
        else
            mMarker.setPosition(new LatLng(location.getLatitude(), location.getLongitude()));
    }

    public void setPosition(LatLng location) {
        if(mMarker == null)
            mMarkerOptions.position(location);
        else
            mMarker.setPosition(location);
    }

    public LatLng getPosition() {
        if(mMarker != null)
            return mMarker.getPosition();

        return mMarkerOptions.getPosition();
    }

    public boolean isMarkerOwner(com.google.android.gms.maps.model.Marker marker) {
        return mMarker.equals(marker);
    }

    public void select() {
        mSelected = true;

        if(mMarker == null)
            mMarkerOptions.icon(BitmapDescriptorFactory.fromBitmap(getSelectedMarkerBitmap()));
        else
            mMarker.setIcon(BitmapDescriptorFactory.fromBitmap(getSelectedMarkerBitmap()));
    }

    public void deselect() {
        mSelected = false;

        if(mMarker == null)
            mMarkerOptions.icon(BitmapDescriptorFactory.fromBitmap(getMarkerBitmap()));
        else
            mMarker.setIcon(BitmapDescriptorFactory.fromBitmap(getMarkerBitmap()));
    }

    public void toggle() {
        if(mSelected)
            deselect();
        else
            select();
    }

    public void setSelectState(boolean state) {
        if(state)
            select();
        else
            deselect();
    }

    public boolean isSelected() {
        return mSelected;
    }

    private Bitmap getMarkerBitmap() {
        if(mMarkerBitmap == null) {
            mMarkerBitmap = createMarkerBitmap();
        }
        return mMarkerBitmap;
    }

    private Bitmap getSelectedMarkerBitmap() {
        if(mSelectedMarkerBitmap == null) {
            mSelectedMarkerBitmap = createSelectedMarkerBitmap();
        }
        return mSelectedMarkerBitmap;
    }

    /**
     * Creates marker bitmap
     *
     * @return
     */
    abstract protected Bitmap createMarkerBitmap();

    /**
     * Creates bitmap for a selected marker
     *
     * @return
     */
    abstract protected Bitmap createSelectedMarkerBitmap();
}
