package me.magicdevel.here.marker;

import com.google.android.gms.maps.model.LatLng;

import java.util.LinkedList;

/**
 * Created by magicdevel on 16.07.2014.
 */
public class MarkersCollection extends LinkedList<Marker> {
    public Marker findByMapMarker(com.google.android.gms.maps.model.Marker marker) {
        for (Marker m : this) {
            if(m.isMarkerOwner(marker)) {
                return m;
            }
        }
        return null;
    }

    public Marker findMarkerByLocation(LatLng location) {
        for (Marker m : this) {
            if(m.getPosition().equals(location)) {
                return m;
            }
        }
        return null;
    }

    @Override
    public boolean remove(Object object) {
        boolean removed = super.remove(object);

        ((Marker)object).removeFromMap();

        return removed;
    }
}
