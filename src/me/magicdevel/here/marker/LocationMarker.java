package me.magicdevel.here.marker;

import android.graphics.Bitmap;

/**
 * Created by magicdevel on 16.07.2014.
 */
public class LocationMarker extends Marker {
    @Override
    protected Bitmap createMarkerBitmap() {
        return MarkerBitmapFactory.instance().getLocation();
    }

    @Override
    protected Bitmap createSelectedMarkerBitmap() {
        return MarkerBitmapFactory.instance().getSelectedLocation();
    }
}
