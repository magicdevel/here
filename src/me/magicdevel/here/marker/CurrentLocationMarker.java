package me.magicdevel.here.marker;

import android.graphics.Bitmap;

/**
 * Created by magicdevel on 15.07.2014.
 */
public class CurrentLocationMarker extends Marker {
    @Override
    protected Bitmap createMarkerBitmap() {
        return MarkerBitmapFactory.instance().getCurrentLocation();
    }

    @Override
    protected Bitmap createSelectedMarkerBitmap() {
        return MarkerBitmapFactory.instance().getSelectedCurrentLocation();
    }
}
