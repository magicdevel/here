package me.magicdevel.here.parcel;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.maps.model.LatLng;
import me.magicdevel.here.marker.*;

import java.util.Calendar;
import java.util.LinkedList;

/**
 * Created by magicdevel on 21.07.2014.
 */
public class MapFragmentParcel implements Parcelable {
    private LatLng mCameraPosition = new LatLng(0, 0);
    private float mZoom = 0;
    private LinkedList<MarkerParcel> mMarkers = new LinkedList<MarkerParcel>();
    private long mSaveTime = 0;

    public LatLng getCameraPosition() {
        return mCameraPosition;
    }

    public void setCameraPosition(LatLng cameraPosition) {
        this.mCameraPosition = cameraPosition;
    }

    public float getZoom() {
        return mZoom;
    }

    public void saveTime() {
        Calendar c = Calendar.getInstance();
        mSaveTime = c.getTimeInMillis();
    }

    public long getSaveTime() {
        return mSaveTime;
    }

    public void setZoom(float zoom) {
        this.mZoom = zoom;
    }

    public void setMarkers(MarkersCollection markers) {
        mMarkers.clear();

        for(Marker marker : markers) {
            mMarkers.add(new MarkerParcel(marker));
        }
    }

    public MarkersCollection getMarkers() {
        MarkersCollection markers = new MarkersCollection();

        for(MarkerParcel parcel : mMarkers) {
            Marker marker = parcel.createMarker();

            if(marker != null)
                markers.add(marker);
        }

        return markers;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        writeSaveTime(dest);
        writeCameraPosition(dest);
        writeZoom(dest);
        writeMarkers(dest);
    }

    protected void writeSaveTime(Parcel dest) {
        dest.writeLong(mSaveTime);
    }

    protected void writeCameraPosition(Parcel dest) {
        dest.writeParcelable(mCameraPosition, 0);
    }

    protected void writeZoom(Parcel dest) {
        dest.writeFloat(mZoom);
    }

    protected void writeMarkers(Parcel dest) {
        dest.writeInt(mMarkers.size());

        for(MarkerParcel parcel : mMarkers) {
            dest.writeInt(parcel.getType());
            dest.writeInt(parcel.isSelected() ? 1 : 0);
            dest.writeParcelable(parcel.getPosition(), 0);
            dest.writeLong(parcel.getHistoryId());
        }
    }

    public static final Parcelable.Creator<MapFragmentParcel> CREATOR
            = new Parcelable.Creator<MapFragmentParcel>() {

        public MapFragmentParcel createFromParcel(Parcel in) {
            MapFragmentParcel parcel = new MapFragmentParcel();

            readSaveTime(in, parcel);
            readCameraPosition(in, parcel);
            readZoom(in, parcel);
            readMarkers(in, parcel);

            return parcel;
        }

        public MapFragmentParcel[] newArray(int size) {
            return new MapFragmentParcel[size];
        }

        protected void readSaveTime(Parcel in, MapFragmentParcel parcel) {
            parcel.mSaveTime = in.readLong();
        }

        protected void readCameraPosition(Parcel in, MapFragmentParcel parcel) {
            parcel.mCameraPosition = in.readParcelable(MapFragmentParcel.class.getClassLoader());
        }

        protected void readZoom(Parcel in, MapFragmentParcel parcel) {
            parcel.mZoom = in.readFloat();
        }

        protected void readMarkers(Parcel in, MapFragmentParcel parcel) {
            int markersCount = in.readInt();

            for (int i = 0; i < markersCount; i++) {
                int type = in.readInt();
                boolean selected = in.readInt() == 1;
                LatLng position = in.readParcelable(MapFragmentParcel.class.getClassLoader());
                long historyId = in.readLong();

                parcel.mMarkers.add(new MarkerParcel(type, selected, position, historyId));
            }
        }
    };

    static class MarkerParcel {
        /**
         * Обычный маркер
         */
        public static final int MARKER = 0;
        /**
         * Целевой маркер
         */
        public static final int TARGET_MARKER = 1;
        /**
         * Маркер текущего положения
         */
        public static final int CURRENT_LOCATION_MARKER = 2;
        /**
         * Маркер избранного положения
         */
        public static final int FAVORITE_LOCATION_MARKER = 3;

        private int mType;
        private LatLng mPosition;
        private boolean mSelected;
        private long mHistoryId;

        public MarkerParcel(int type, boolean selected, LatLng position, long historyId) {
            mType = type;
            mSelected = selected;
            mPosition = position;
            mHistoryId = historyId;
        }

        public MarkerParcel(Marker marker) {
            if(marker instanceof TargetLocationMarker) {
                mType = TARGET_MARKER;
                mHistoryId = ((TargetLocationMarker)marker).getHistoryId();
            } else if(marker instanceof CurrentLocationMarker) {
                mType = CURRENT_LOCATION_MARKER;
            } else if(marker instanceof FavoriteLocationMarker) {
                mType = FAVORITE_LOCATION_MARKER;
            } else {
                mType = MARKER;
            }

            mPosition = marker.getPosition();
            mSelected = marker.isSelected();
        }

        public LatLng getPosition() {
            return mPosition;
        }

        public int getType() {
            return mType;
        }

        public boolean isSelected() {
            return mSelected;
        }

        public void setHistoryId(long id) {
            mHistoryId = id;
        }

        public long getHistoryId() {
            return mHistoryId;
        }

        public Marker createMarker() {
            Marker marker;

            switch (getType()) {
                case CURRENT_LOCATION_MARKER:
                    marker = new CurrentLocationMarker();
                    break;
                case FAVORITE_LOCATION_MARKER:
                    marker = new FavoriteLocationMarker();
                    break;
                case MARKER:
                    marker = new LocationMarker();
                    break;
                case TARGET_MARKER:
                    marker = new TargetLocationMarker();
                    ((TargetLocationMarker)marker).setHistoryId(mHistoryId);
                    break;
                default:
                    return null;
            }

            marker.setPosition(getPosition());
            if(mSelected)
                marker.select();

            return marker;
        }
    }
}
