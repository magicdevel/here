package me.magicdevel.here.parcel;

import android.os.Parcelable;

/**
 * Created by magicdevel on 21.07.2014.
 */
public interface HasParcel {
    public Parcelable getParcel();

    public String getParcelName();

    public void applyParcel(Parcelable parcel);
}
